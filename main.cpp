#include <QtCore/QCoreApplication>
#include <QtCore/QTimer>

#include <Soprano/Statement>
#include <Soprano/Node>
#include <Soprano/Model>
#include <Soprano/QueryResultIterator>
#include <Soprano/StatementIterator>
#include <Soprano/NodeIterator>
#include <Soprano/PluginManager>

#include <Nepomuk/Resource>
#include <Nepomuk/ResourceManager>
#include <Nepomuk/Variant>
#include <Nepomuk/File>
#include <Nepomuk/Tag>

#include <Nepomuk/Types/Property>
#include <Nepomuk/Types/Class>

#include <Nepomuk/Query/Query>
#include <Nepomuk/Query/FileQuery>
#include <Nepomuk/Query/ComparisonTerm>
#include <Nepomuk/Query/LiteralTerm>
#include <Nepomuk/Query/ResourceTerm>
#include <Nepomuk/Query/QueryServiceClient>
#include <Nepomuk/Query/ResourceTypeTerm>
#include <Nepomuk/Query/QueryParser>
#include <Nepomuk/Query/Result>

#include <KDebug>

// Vocabularies
#include <Soprano/Vocabulary/RDF>
#include <Soprano/Vocabulary/RDFS>
#include <Soprano/Vocabulary/NRL>
#include <Soprano/Vocabulary/NAO>

#include <Nepomuk/Vocabulary/NIE>
#include <Nepomuk/Vocabulary/NFO>
#include <Nepomuk/Vocabulary/NMM>
#include <Nepomuk/Vocabulary/NCO>
#include <Nepomuk/Vocabulary/PIMO>

#include <KUrl>
#include <KJob>
#include <nepomuk/datamanagement.h>
#include <iostream>

using namespace Soprano::Vocabulary;
using namespace Nepomuk::Vocabulary;

class TestObject : public QObject {
    Q_OBJECT
public slots:
    void main();

public:
    TestObject() {
        QTimer::singleShot( 0, this, SLOT(main()) );
    }
};

int main( int argc, char ** argv ) {
    if( Nepomuk::ResourceManager::instance()->init() ) {
        qDebug() << "Nepomuk isn't running";
        return 0;
    }

    KComponentData component( QByteArray("nepomuk-test") );
    QCoreApplication app( argc, argv );

    TestObject a;
    app.exec();
}

void TestObject::main()
{
    // --- CODE

    using namespace Nepomuk;
    using namespace Nepomuk::Vocabulary;

    QMultiHash<QString, QString> m_tagRepo;
    m_tagRepo.insert( QLatin1String("file:///home/vishesh/mb.sh"), QLatin1String("script") );
    qDebug() << "I am in ; ";

    QStringList resourceNames = m_tagRepo.uniqueKeys();

    qDebug() << "Resource Names List: " << resourceNames;

    foreach(QString resourceName, resourceNames) {
        qDebug() << " Resource Name: " << resourceName;

        KUrl kurl(resourceName);
        qDebug() << "KUrl : " << kurl.url();

        Nepomuk::File resourceFile( kurl );

        qDebug() << "Resource File Created: " << resourceFile.url().url();
        QStringList tagNameList = m_tagRepo.values(resourceName);

        qDebug() << "Tag Names: " << tagNameList;

        foreach(QString tagName, tagNameList) {
            Nepomuk::Tag tag( tagName );
            resourceFile.addTag( tag );

            kDebug() << "Resource tagged";
            qDebug() << "Tags: " << resourceFile.tags();
        }
    }
    QCoreApplication::instance()->quit();
}

#include "main.moc"